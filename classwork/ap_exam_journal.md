| Day | Studied |
| ----------- | ----------- |
| 3/28 | Looked up and memorized definitions of **Citizen Science**, **Rogue Access Points**, and **Crowdsourcing** |
| 3/30 | Went through the **Impact of Computing** Lesson on Khan Academy |
| 4/1 | Looked through cool projects on Github to practice reverse-engineering functions ([This one](https://github.com/alacritty/alacritty) was cool)
