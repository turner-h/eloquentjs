# AP Exam Practice Questions
## Searching List for a Value
This question I got wrong because the question was wrong. The "correct" answer says that every option functions correctly, however this is not the case.
Procedure B is the following:
```js
PROCEDURE B(aList)
{
i ← 1
len ← LENGTH(aList)
REPEAT len TIMES
{
      IF(aList[i]  = 23)  
      {
         RETURN(i)
      }
}
RETURN ("-1")
}
```

This function repeats as many times as there are items in the list, however ``i`` is never iterated, so it will only ever check the first item in the list.
If the list looked like the following:
```js 
aList = [20, 23, 23, 23, 23];
```
The function would return ``-1`` instead of ``2``, the index of the first ``23``. This is not the intended result, so the answer is wrong.
## Number of Iterations for Binary Search
I answered this question incorrectly because my train of thought was a little too naive and quick.
The correct answer was that the maximum numbers of iterations for a set of 2000 values would be 11.
I thought that 1000 made sense because it was half of 2000. (Half made sense because of binary) However a binary search looks more like this:
```
-----|-----
--|--|-----
--|-||-----
--|*||-----
```
(``-`` = unsearched value, ``|`` = searched value, ``*`` = target value)

This searches 4 values instead of 6, and this effect is only amplified at larger scales.
## Calculating the Range of a Dataset
My main problem with this question was that I forgot that arrays start at ``[1]`` in psuedocode. I thought that they started at ``[0]``.
What ruled out Answer D for me was that they accessed an array with parenthesis:
```js
last = dataset(number)
```
instead of with square brackets:
```js
last = dataset[number]
```
This is not addressed in the solution, however I am guessing that it is isn't correct.
The main takeaway from this question is that pseudocode arrays start at 1.
## Basketball Handedness
The biggest issue for me here was that I read the question wrong. I answered which players were right handed instead of left handed.
19 in binary is ``10011``, and ``0`` represents a lefthanded player. In this case, the zeros line up with Shooting Guard and Small forward in this list:
```
point guard, shooting guard, small forward, power forward, center
```
As I said, I understood all of this, I just read the question incorrectly and found all of the right-handed players.
## Citizen Science Example
I did not know what citizen science was when I took the test, so that is why I got this question wrong. Essentially, citizen science is science that is either contributed to by non-scientists, or the volunteers will donate their computing power to help.

In this example, the state agency is doing the most citizen science as it is asking volunteers (instead of scientists) to count and classify birds for their database.

My answer was wrong, because the soda company was not conducting a study, but merely running a poll to put on their website.
## List Operations
For this question, I do not remember my thought process, however I do know what the correct answer is now.

The list orginally contains the following values:
```
[1, 3, 5, 2, 4]
```
If we then ``REMOVE()`` the second element in that array, we get the following:
```
[1, 5, 2, 4]
```
We the ``APPEND(5)`` to the list, which adds ``5`` to the end:
```
[1, 5, 2, 4, 5]
```
And then ``11`` is inserted at index ``3``:
```
[1, 5, 11, 2, 4, 5]
```
We then add the first and seconds elements of the array, as well as the length, and we end up with 12:
```
1 + 5 + 6 = 12
```
I do not know how I got 18 orginally, but this is how the question works and I understand how to do it.
