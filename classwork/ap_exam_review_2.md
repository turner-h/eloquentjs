# AP Exam Review 2
## Current Value
### Mathematical Expressions
This question I got wrong because I didn't write it down. I tried to do all of the calculations in my head and that ended poorly. The question starts by defining ``a`` and ``b``:
```js
a = 3
b = 5
```
It then performs some calulations using these numbers
```js
a = a + b
b = a + b
c = a + b
```
Replacing these variables with the numbers they represent helps us follow the change in values:
```js
a = 3 + 5
b = 8 + 5
c = 8 + 13
```
At this point, ``c`` is equal to ``21``, which is displayed at the end of the program.

My main issue with this question is that I didn't write it down and tried to do all of the calulations in my head, and I got lost.

## Free Movie Conditional Statement
### Conditionals
I am beginning to see a pattern here where I understand how to do the questions, but I fail to read the question thouroughly. I will try a lot harder on reading comprehension next attempt.
This question is no different: I understood how to do this question but I missed that I needed to pick to answers.

The conditional statement is the following:
```js
freeMovie ← false
IF ( ( (age > 12) AND (age < 20) ) OR ( (seasonPass = "Silver") AND (age > 60) ) )
{
    freeMovie ← true
}
```
Essentially, this conditional determines if 

1. The customer is over 12 and under 20 (a teenager)
2. Or is the customer is over 60 and owns a "Silver" season pass

These are the two answers, I just missed the part where I needed two answers.

## Most Benefitted from Using Distributed Computing
### Fault Tolerance
I did not really understand what distributed computing was when I did this question, which is why I got it wrong. Essentially, distributed computing is a bunch of computers on a network that can send data back and forth. This is very useful when you want to create data on one computer and use it on another one.
My biggest misconception about this was that it would be useful for calculations. While that is sort of true, computers are really good at calculations on their own. 

The other two answers, being ``Controlling the aircraft that depart from and arrive at an international airport`` and ``Operating an online game with hundreds of virtual players participating``, make much more sense because data **has** to be generated on different devices (like the player and the server creating different data in a game).

## Multifactor Authentication Examples
### Safe Computing
This question confused me because every answer had two types of authentication. What I misunderstood was that they have to be *different types* of authentication. I picked the one that made the most sense to me, which was the ``bank card + PIN`` option, because a physical method of authentication could be stolen. However, the correct answer was ``password + PIN``, because they are essentially the same factor. A PIN is basically just a shorter password.
