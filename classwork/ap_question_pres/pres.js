function b(list) { //Procedure B, searches the provided list for 23
	let i = 0;
	let len = list.length;

	for(let j = 0; j < len; j++) { //j is not used, it is only there to imitate the REPEAT len TIMES loop
		if(list[i] == 23) {
			return i;
		}
	}
	return -1;
}

let list = [20, 23, 23, 23, 23];
console.log(b(list));
