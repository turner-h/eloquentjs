## Searching a List for a Value
### Conditionals
``Consider the following procedures (A, B, and C) that are all designed to search the list aList for the value 23 and return the index in the list where 23 is found.  If the number 23 is not in the list, -1 should be returned.``

All this question wants you to do is determine if all of the procedures work and if some are more efficient.

Here is the correct answer:

``All three procedures work correctly, but A and B are more efficient (on average) than C.``

This means that every function should work correctly, including procedure B. ***They do not.***
The biggest reason I got this question wrong is because procedure B does not work correctly.

Here is procedure B:
```js
PROCEDURE B(aList)
{
i ← 1
len ← LENGTH(aList)
REPEAT len TIMES
{
      IF(aList[i]  = 23)  
      {
         RETURN(i)
      }
}
RETURN ("-1")
}
```

Here is the explanation for why procedure B works:

``Procedure B works correctly.  A variable i is initially assigned a value of 1 and incremented every pass in the loop.    The loop will iterate (up to) the same number of times as the length of the list.  If the value at index i is ever 23, i is returned.``

The variable ``i`` is never actually iterated in the procedure we are shown, and I will show you how the procedure does not work correctly.
