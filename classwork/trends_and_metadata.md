## Data
### Turner, Andy, and Jack

### Gyms
Gyms are searched more frequently during January. This is most likely because people are making New Year's Resolutions. Gyms also peak slightly during the summer, potentially due to having more free time.

### Deer
Searches for deer peak HEAVILY during November every year. This is probably due to hunting season being open for deer.

### MLK
Martin Luther King Jr. is searched frequently on Martin Luther King Jr. day.

### DJUNGELSKOG vs Ikea Bear
Ikea sells a stuffed bear called a DJUNGELSKOG, and apparently most people don't search DJUNGELSKOG, they just search Ikea Bear.
