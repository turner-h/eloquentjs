# Internet Notes
### Turner, Alex, Andy, Jack

## The Internet
The Internet is people sending things to other people very far away, quickly

## What Happens When You Go To a Website
1. Your computer talks to a DNS server to get the IP address of the domain name 
2. Your computer makes a GET HTTP request to the IP address of the website
3. The web server sends back a response containing the HTML data
4. Your browser renders the HTML data into a website

## Packets
- Packets are small bits of data
- You want as much data sent as possible

## Servers
- A server is a big server
- A server is a computer that doesn't run userfocused code
- Can send data to users over the internet 
