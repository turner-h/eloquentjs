import random

iterations = 10
count_right = 0

for i in range(iterations):
    num1 = random.randrange(10)
    num2 = random.randrange(10)

    answer = input("What is {} * {}? ".format(num1, num2))
    if answer == num1 * num2:
        print("You aren't braindead, congrats that's right. ")
        count_right += 1
    else:
        print("No you bumbling buffoon, it's {}. ".format(num1 * num2))

print("\nI asked you {} questions. You got {} correct.\nNifty. ".format(iterations, count_right))